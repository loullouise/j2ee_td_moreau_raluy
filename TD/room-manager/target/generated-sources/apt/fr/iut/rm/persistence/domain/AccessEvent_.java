package fr.iut.rm.persistence.domain;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(AccessEvent.class)
public abstract class AccessEvent_ {

	public static volatile SingularAttribute<AccessEvent, Date> date;
	public static volatile SingularAttribute<AccessEvent, String> name;
	public static volatile SingularAttribute<AccessEvent, Long> id;
	public static volatile SingularAttribute<AccessEvent, EventType> type;
	public static volatile SingularAttribute<AccessEvent, Room> room;

}

