package fr.iut.rm;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.UnitOfWork;
import com.google.inject.persist.jpa.JpaPersistModule;
import fr.iut.rm.persistence.dao.AccessEventDao;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.AccessEvent;
import fr.iut.rm.persistence.domain.EventType;
import fr.iut.rm.persistence.domain.Room;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Entry point for command-line program. It's mainly a dumb main static method.
 */
public final class App {
    /**
     * quit constant
     */
    private static final String QUIT = "q";
    /**
     * help constant
     */
    private static final String HELP = "h";
    /**
     * description constant
     */
    private static final String DESCRIPTION = "d";
    /**
     * create constant
     */
    private static final String CREATE = "c";
    /**
     * list constant
     */
    private static final String LIST = "l";

    private static final String ENTER = "e";

    private static final String EXIT = "x";

    private static final String LISTACCESS = "la";

    /**
     * standard logger
     */
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    /**
     * the available options for CLI management
     */
    private final Options options = new Options();
    /**
     * Unit of work is used to drive DB Connection
     */
    @Inject
    UnitOfWork unitOfWork;
    /**
     * Data Access Object for rooms
     */
    @Inject
    RoomDao roomDao;

    @Inject
    AccessEventDao accessEvent;

    /**
     * Invoked at module initialization time
     */
    public App() {
        // build options command line options
        options.addOption(OptionBuilder.withDescription("List all rooms").create(LIST));
        options.addOption(OptionBuilder.withDescription("List all access").create(LISTACCESS));
        options.addOption(OptionBuilder.withArgName("name").hasArg().withDescription("Create new room").create(CREATE));
        options.addOption(OptionBuilder.withArgName("description").hasArg().withDescription("set a description").create(DESCRIPTION));
        options.addOption(OptionBuilder.withArgName("name").hasArg().withDescription("Enter a room").create(ENTER));
        options.addOption(OptionBuilder.withArgName("name").hasArg().withDescription("Exit a room").create(EXIT));
        options.addOption(OptionBuilder.withDescription("Display help message").create(HELP));
        options.addOption(OptionBuilder.withDescription("Quit").create(QUIT));
    }

    /**
     * Displays all the rooms content in DB
     */
    private void showRooms() {
        unitOfWork.begin();

        List<Room> rooms = roomDao.findAll();
        if (rooms.isEmpty()) {
            System.out.println("No room");
        } else {
            System.out.println("Rooms :");
            System.out.println("--------");
            for (Room room : rooms) {
                System.out.println(String.format("   [%d], name '%s', description '%s'", room.getId(), room.getName(), room.getDescription()));
            }
        }

        unitOfWork.end();
    }

    private void showAccess(){
        unitOfWork.begin();
        List<AccessEvent> access = accessEvent.findAll();
        if (access.isEmpty()) {
            System.out.println("No access");
        } else {
            System.out.println("Access :");
            System.out.println("--------");
            for (AccessEvent ae : access) {
                System.out.println(String.format("   [%d], name '%s', room '%s', type '%s'", ae.getId(), ae.getName(), ae.getRoom().getName(),ae.getType()));
            }
        }

        unitOfWork.end();
    }

    /**
     * Creates a room in DB
     *
     * @param name the name of the room
     */
    private void createRoom(final String name) {
        unitOfWork.begin();

        // TODO check unicity

        Room room = new Room();
        room.setName(name);
        roomDao.saveOrUpdate(room);
        unitOfWork.end();
    }

    private void enterARoom(final String name){
        unitOfWork.begin();

        AccessEvent ae = new AccessEvent();
        Room r = roomDao.findByName(name);

        ae.setName(name);
        ae.setDate(new Date());
        ae.setRoom(r);
        ae.setType(EventType.IN);

        unitOfWork.end();

    }

    private void exitARoom(final String name) {
        unitOfWork.begin();

        AccessEvent ae = new AccessEvent();
        Room r = roomDao.findByName(name);

        ae.setName(name);
        ae.setDate(new Date());
        ae.setRoom(r);
        ae.setType(EventType.OUT);

        unitOfWork.end();
    }

    private void setDescription(final String description){
        unitOfWork.begin();

        Room r = roomDao.findAll().get(roomDao.findAll().size()-1);
        r.setDescription(description);
        roomDao.saveOrUpdate(r);
        unitOfWork.end();
    }
    /**
     * Displays help message
     */
    private void showHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("room-manager.jar", options);
    }

    /**
     * Main program entry point
     *
     * @param args main program args
     */
    public static void main(final String[] args) {
        logger.info("Room-Manager version {} started", Configuration.getVersion());
        logger.debug("create guice injector");
        Injector injector = Guice.createInjector(new JpaPersistModule("room-manager"), new MainModule());
        logger.info("starting persistency service");
        PersistService ps = injector.getInstance(PersistService.class);
        ps.start();


        App app = injector.getInstance(App.class);
        app.showHelp();
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = null;
        Scanner sc = new Scanner(System.in);
        do {
            String str = sc.nextLine();
            try {
                cmd = parser.parse(app.options, str.split(" "));
                if (cmd.hasOption(HELP)) {
                    app.showHelp();
                } else if (cmd.hasOption(LIST)) {
                    app.showRooms();
                } else if (cmd.hasOption(LISTACCESS)) {
                        app.showAccess();
                } else if (cmd.hasOption(CREATE)) {
                    String name = cmd.getOptionValue(CREATE);
                    if (name != null && !name.isEmpty()) {
                        app.createRoom(name);
                    }
                } else if (cmd.hasOption(DESCRIPTION)) {
                    String description = cmd.getOptionValue(DESCRIPTION);
                    if(description != null && !description.isEmpty()){
                        app.setDescription(description);
                    }
                } else if (cmd.hasOption(ENTER)) {
                    String roomName = cmd.getOptionValue(ENTER);
                    if(roomName != null && !roomName.isEmpty())
                        app.enterARoom(roomName);
                } else if (cmd.hasOption(EXIT)) {
                    String roomName = cmd.getOptionValue(EXIT);
                    if(roomName != null && !roomName.isEmpty())
                        app.exitARoom(roomName);
                }

            } catch (ParseException e) {
                e.printStackTrace();
                app.showHelp();
            }
        } while (!cmd.hasOption(QUIT));

        logger.info("Program finished");
        System.exit(0);
    }

}
