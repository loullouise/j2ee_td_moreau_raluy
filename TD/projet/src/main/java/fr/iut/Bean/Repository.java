package fr.iut.Bean;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;

public class Repository {
    @NotNull@URL(protocol = "http")
    private
    String url;

    public Repository(){
        setUrl("");
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
