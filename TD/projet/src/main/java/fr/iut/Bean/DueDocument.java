package fr.iut.Bean;

import javax.validation.constraints.Future;
import java.util.Date;

public class DueDocument extends Document {
    @Future
    private Date dueDate;

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public DueDocument(){
        dueDate = new Date();
    }
}
