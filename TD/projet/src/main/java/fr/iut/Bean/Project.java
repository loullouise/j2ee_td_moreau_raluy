package fr.iut.Bean;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.Set;

public class Project {


    private Collection<Document> documents;

    @NotNull
    private
    Repository repository;

    @NotNull
    private
    Set<Person> customers;

    @NotNull
    private
    Set<Person> students;

    @NotNull
    private
    Set<Person> teachers;

    @NotNull
    private
    String description;

    @NotNull
    private
    String name;


    public Project(){
        repository = new Repository();
        description = "";
        name = "";
    }


    public Collection<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(Collection<Document> documents) {
        this.documents = documents;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public Set<Person> getCustomers() {
        return customers;
    }

    public void setCustomers(Set<Person> customers) {
        this.customers = customers;
    }

    public Set<Person> getStudents() {
        return students;
    }

    public void setStudents(Set<Person> students) {
        this.students = students;
    }

    public Set<Person> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<Person> teachers) {
        this.teachers = teachers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
