package fr.iut.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LoginValidator  implements ConstraintValidator<Login,String>{

    private final static int MINIMAL_LENGTH = 2;
    private final static int MAXIMAL_LENGTH = 8;

    @Override
    public void initialize(Login login) {

    }

    @Override
    public boolean isValid(final String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s.length() >= MINIMAL_LENGTH && s.length() <= MAXIMAL_LENGTH && s.matches("[a-zA-Z]+"))
            return true;
        else
            return false;
    }
}


