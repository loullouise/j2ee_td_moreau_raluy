package fr.iut;

import fr.iut.Bean.DueDocument;
import fr.iut.Bean.Person;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Set;

public class DueDocumentTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        DueDocumentTest.validator = factory.getValidator();
    }

    @Test
    public void testGoodDueDocument() throws ParseException {
        DueDocument d = new DueDocument();
        d.setDueDate(new SimpleDateFormat("dd/MM/yyyy").parse("09/02/2020"));

        Set<ConstraintViolation<DueDocument>> violations = validator.validate(d);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void testBadDueDocument() throws ParseException {
        DueDocument d = new DueDocument();
        d.setDueDate(new SimpleDateFormat("dd/MM/yyyy").parse("09/02/2000"));

        Set<ConstraintViolation<DueDocument>> violations = validator.validate(d);
        Assert.assertEquals(1, violations.size());
    }
}
