package fr.iut;

import fr.iut.Bean.Document;
import fr.iut.Bean.Person;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Set;

public class DocumentTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        DocumentTest.validator = factory.getValidator();
    }

    @Test
    public void testGoodDateTime() throws ParseException {
        Document d = new Document();
        d.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("10/07/2003"));
        d.setLastModificationDate(new SimpleDateFormat("dd/MM/yyyy").parse("09/02/2014"));


        Set<ConstraintViolation<Document>> violations = validator.validate(d);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void notNullStrings() throws ParseException {
        Document d = new Document();
        d.setTitle("Test");
        d.setContent("Test document");
        d.setCreator(new Person());
        d.setLastModifier(new Person());
        d.setCreationDate(new SimpleDateFormat("dd/MM/yyyy").parse("10/07/2003"));
        d.setLastModificationDate(new SimpleDateFormat("dd/MM/yyyy").parse("09/02/2014"));

        Set<ConstraintViolation<Document>> violations = validator.validate(d);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void nullStrings(){
        Document d = new Document();
        d.setTitle(null);
        d.setContent(null);
        d.setCreator(null);
        d.setLastModifier(null);

        Set<ConstraintViolation<Document>> violations = validator.validate(d);
        Assert.assertEquals(4, violations.size());
    }

}
