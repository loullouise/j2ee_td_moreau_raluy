package fr.iut;

import fr.iut.Bean.Person;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class PersonTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        PersonTest.validator = factory.getValidator();
    }

    @Test
    public void testGoodLogin() {
        Person p = new Person();
        p.setLogin("Test");

        Set<ConstraintViolation<Person>> violations = validator.validate(p);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void testBadLogin() {
        Person p = new Person();
        p.setLogin("textedeplusdehuitcaracteres");

        Set<ConstraintViolation<Person>> violations = validator.validate(p);
        Assert.assertEquals(1, violations.size());
    }

    @Test
    public void testBadLogin2() {
        Person p = new Person();
        p.setLogin("test123");

        Set<ConstraintViolation<Person>> violations = validator.validate(p);
        Assert.assertEquals(1, violations.size());
    }

    @Test
    public void testGoodMail() {
        Person p = new Person();
        p.setEmail("test@test.fr");
        p.setLogin("testLog");

        Set<ConstraintViolation<Person>> violations = validator.validate(p);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void testBadMail() {
        Person p = new Person();
        p.setEmail("test");
        p.setLogin("testLog");

        Set<ConstraintViolation<Person>> violations = validator.validate(p);
        Assert.assertEquals(1, violations.size());
    }

    @Test
    public void notNullStrings() {
        Person p = new Person();
        p.setFirstname("Toto");
        p.setLastname("Dupond");
        p.setEmail("test@test.fr");
        p.setLogin("testLog");

        Set<ConstraintViolation<Person>> violations = validator.validate(p);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void nullStrings() {
        Person p = new Person();
        p.setFirstname(null);
        p.setLastname(null);
        p.setEmail("test");
        p.setLogin("testLogin33000");

        Set<ConstraintViolation<Person>> violations = validator.validate(p);
        Assert.assertEquals(4, violations.size());
    }
}
