![1426474615_stroke_letter_r_uppercase_gestureworks.png](https://bitbucket.org/repo/L8dEAG/images/2246400225-1426474615_stroke_letter_r_uppercase_gestureworks.png)

Corentin MOREAU / Louise RALUY

# README #

This project is a room manager, according to the J2EE courses from IUT de Bordeaux in France.

### Requirements ###

* Tomcat installed on your server.
* Maven
* In your tomcat-users.xml, you need to add this user: `<user username="fred" password="fred" roles="rm-admin"/>`
* 8080 port opened

### Installation ###

* Clone this project
* Use the script `deploy.sh` in `room-manager-projet` folder to deploy this webapp in your tomcat server
*  You can add `--assembly` if assembly failed and you need to re-assembly it
*  You can add `--nobrowser` parameter if you don't want to open the browser after deployement
* Build Logs are stored in the `build.log` file.
* Assembly logs are stored in the `assembly.log` file.

### One more thing ###

The report, asked for the end of the project is the file named `rapport-moreau-raluy-room-manager.pdf` located in the  `room-manager-projet` folder.