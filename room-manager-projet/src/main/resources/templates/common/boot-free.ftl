<#include "assets/header-common.ftl">

<div class="container">
    <div class="row-fluid">
        <div class="jumbotron"><h2><center>${description}</center></h2><br/>
        <img class="center-block" src="img/room-manager-logo.png"/></div>
</div>
</div> <!-- /container -->

<#include "assets/end-common.ftl">
