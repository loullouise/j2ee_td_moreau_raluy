<#include "assets/header-common.ftl">

<div class="container">
    <div class="row clearfix">
        <#list rooms as room>
            <div class="col-md-4">
                <#if (room.getRatio() < 40)>
                    <div class="panel panel-success">
                <#elseif (room.getRatio() >= 40 && room.getRatio() < 70)>
                    <div class="panel panel-warning">
                <#elseif (room.getRatio() >= 70)>
                    <div class="panel panel-danger">
                </#if>
                    <div class="panel-heading text-center">
                <h2>${room.getName()}</h2>
                    </div>
                    <div class="panel-body text-center">
                        <p>People in this room: ${room.getPeople()}</p>
                        <p>Capacity: ${room.getCapacity()}</p>
                    </div>
                    <div class="panel-footer">
                        <div class="btn-group" role="group" aria-label="myGroup">
                            <div class="btn-group pull-left" role="group">
                                <a href="io?name=${room.getName()}&type=in" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                            </div>
                            <div class="btn-group pull-left" role="group">
                                <a href="io?name=${room.getName()}&type=out" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                            </div>
                        </div>
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                QRCode <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="qrcode?name=${room.getName()}&type=in">In QRCode</a></li>
                                <li><a href="qrcode?name=${room.getName()}&type=out">Out QRCode</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </#list>
    </div>
</div> <!-- /container -->

<#include "assets/end-common.ftl">
