package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet simply
 */
@Singleton
public class InitServlet extends HttpServlet {
    /**
     * the dao used to access room persisted data
     */
    @Inject
    RoomDao roomDao;

    private static int CALLED_COUNTER = 0;

    /**
     * HTTP GET access
     * @param req use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws ServletException by container
     * @throws IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {

        if(CALLED_COUNTER == 0){
            Room room = new Room();
            room.setName("202");
            room.setCapacity(24);
            roomDao.saveOrUpdate(room);

            Room room1 = new Room();
            room1.setName("203");
            room1.setCapacity(30);
            room1.setPeople(28);
            roomDao.saveOrUpdate(room1);

            Room room2 = new Room();
            room2.setName("Amphithéâtre 4");
            room2.setCapacity(120);
            room2.setPeople(84);
            roomDao.saveOrUpdate(room2);

            Room room3 = new Room();
            room3.setName("311");
            room3.setCapacity(47);
            roomDao.saveOrUpdate(room3);

            Room room4 = new Room();
            room4.setName("003");
            room4.setCapacity(30);
            room4.setPeople(17);
            roomDao.saveOrUpdate(room4);

            resp.getOutputStream().print("<!DOCTYPE html>\n" +
                    "<html>\n" +
                    " <head>\n" +
                    "  <meta charset=\"UTF-8\">\n" +
                    "  <title>title</title>\n" +
                    " </head>\n" +
                    " <body>\n" +
                    "</html>Some new random rooms are now in base. Go to rooms list:" +
                    "<a href=\"rooms\">Rooms list</a>" +
                    " </body>");

            InitServlet.CALLED_COUNTER++;
        }else{
            resp.sendRedirect("rooms");
        }



    }
}
