package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Singleton
public class DeleteRoomServlet extends HttpServlet {

    @Inject
    RoomDao roomDao;

    /**
     * Get access
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");

        if(name != null){
            Room r = roomDao.findByName(name);
            roomDao.deleteRoomByName(r);
        }
        response.sendRedirect("home");
    }
}