# I clean your screen !
clear

if [ "$1" = "--assembly" ] || [ "$2" = "--assembly" ]
then
echo "[INFO] Assembly in progress (This operation can take a few time)..."
# Assembly project 
mvn assembly:assembly > assembly.log
fi

echo "[INFO] Packaging webapp (Please Wait)..."

# Package generation
mvn clean package > build.log

echo "[INFO] Package created..."
# Check if Tomcat is already running...
if [ "$(netstat -na | grep 8080)" ]
then
echo "[INFO] Stop current Tomcat server..."
echo "\n\n" >> build.log
# Stop tomcat server
$CATALINA_HOME/bin/shutdown.sh >> build.log
fi
# Deletion of the webapp if exists
if [ -d "$CATALINA_HOME/webapps/room-manager" ]
then
    rm -rf $CATALINA_HOME/webapps/room-manager
fi

echo "[INFO] Copying .war in webapps/"

# copy .war in folder webapps/
cp target/room-manager.war $CATALINA_HOME/webapps/

echo "[INFO] Run Tomcat server..."
echo "\n\n" >> build.log
# parameter --nobrowser ; No parameter: debug mode or test :-)
if [ "$1" = "--nobrowser" ] || [ "$2" = "--nobrowser" ]
then
# Run tomcat 
$CATALINA_HOME/bin/startup.sh >> build.log
else

# System detection
case "$OSTYPE" in
   linux*)
      openThis="xdg-open"
      ;;
   darwin*)
      openThis="open"
      ;;
esac

# run tomcat plus open browser
$CATALINA_HOME/bin/startup.sh >> build.log
$openThis http://127.0.0.1:8080/room-manager
fi
echo "[INFO] Deployment finished ! "